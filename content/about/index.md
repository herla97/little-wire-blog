---
title: "About"
date: 2020-02-05T12:01:12-06:00
draft: false
author: "saherla"
---

<style>
.author {
    width: 250px;
}
</style>

{{< figure src="/images/author.jpg" class="author" alt="author" >}}


## Contact

**Email:** saherla03@gmail.com

**Gitlab:** [herla97](https://gitlab.com/herla97)

**Github:** [herla97](https://github.com/herla97)

**Linkedin:** [saherla](https://www.linkedin.com/in/saherla/)


## About Me

Hi! I'm Salvador Hernández L.
I Biomedical Engineer, web development enthusiast, passionate about technology and shooting games. To be fascinated by web programming, e.g. developing scalable applications and websites, with the use of the best technologies.

Currently, I have specialized using [Golang](https://golang.org/) as my primary language and various frameworks for API development, also still learning new technologies like [NET Core](https://dotnet.microsoft.com/) and [Elixir](https://elixir-lang.org/).



### Technologies
* [Golang](https://golang.org/)
* [Vuejs](https://vuejs.org/)
* [Docker](https://www.docker.com/)
* [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
* [Flutter](https://flutter.dev/)

### Languajes
* Spanish
* English (technical)

***
##### Other thing
If you want to see other topics, I invite you to see the blog of a good friend [Ivan Salazar](https://blog.ivansalazar.org/), he spends it configuring many servers.