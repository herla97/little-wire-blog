---
title: "Install Docker on Windows 10 Home"
date: 2020-03-16T18:50:03-06:00
draft: false
description: "Install Docker on Windows 10 Home without a license"
categories: ["Docker"]
tags: ["Docker", "Windows 10"]

featuredImage: /images/docker-install-win10/header-docker-win10.png
featuredImagePreview: ""

toc: true
autoCollapseToc: true
---

Sometimes we have to install Docker as fast as possible to execute a project, but we find that our version of Windows 10 is **Home**.

But knowing that we try to install Docker and it throws us the following error message:

![Docker Error](/images/docker-install-win10/d1.png)

This occurs because Docker needs [Hyper-V](https://en.wikipedia.org/wiki/Hyper-V) to run on Windows and the basic version doesn't have that feature.

Searching in some [Reddit](https://www.reddit.com/) Post, I found how to Install Docker without the need to buy the Windows Pro or Enterprise license, so it was interesting to share it.

Post viewed:
[Post 1](https://www.reddit.com/r/docker/comments/blks5g/install_docker_on_windows_10_home_edition/) -
[Post 2](https://www.reddit.com/r/docker/comments/eyxynv/install_docker_on_windows_10_home/)

## Before Installing **Docker**
Before starting it is necessary to install Hyper-V and Containers using batch files with ".bat" termination, typical of Microsoft.

### Installing Hyper-V
We will create a `hyper v.bat` file and add the following code to our file.

```bat
pushd "%~dp0"
dir /b %SystemRoot%\servicing\Packages\*Hyper-V*.mum >hyper-v.txt
for /f %%i in ('findstr /i . hyper-v.txt 2^>nul') do dism /online /norestart /add-package:"%SystemRoot%\servicing\Packages\%%i"
del hyper-v.txt
Dism /online /enable-feature /featurename:Microsoft-Hyper-V -All /LimitAccess /ALL
pause
```
With our file ready we run it in administrator mode, you can use PowerShell.

![Hyper-V Install](/images/docker-install-win10/w1.png)

This process takes a little time.

![Hyper-V Installed](/images/docker-install-win10/w2.png)
When it's done, **don't restart your computer**.

### Installing Containers

Like Hyper-V, we will create a file called `container.bat` and add the following code.

```bat
pushd "%~dp0"
dir /b %SystemRoot%\servicing\Packages\*containers*.mum >containers.txt
for /f %%i in ('findstr /i . containers.txt 2^>nul') do dism /online /norestart /add-package:"%SystemRoot%\servicing\Packages\%%i"
del containers.txt
Dism /online /enable-feature /featurename:Containers -All /LimitAccess /ALL
pause
```

We run our file in administrator mode and wait a few minutes while it runs.

![Container Install](/images/docker-install-win10/w3.png)


Once the process is finished, we can restart our computer.

![Container Installed](/images/docker-install-win10/w4.png)

### Editing the Windows Version in Registry

We open the Windows executable using the command **Windows + R** and write `regedit`.

![Registry Editor](/images/docker-install-win10/w5.png)

In the Registry Editor we will look for the following directory.

`\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion`

![Registry Editor](/images/docker-install-win10/w6.png)

We click *EditionID* a window will be displayed where we will change *Value Data* to `Professional`.

![EditionID](/images/docker-install-win10/w7.png)

We accept.

> You shouldn't restart the computer if you do, we will have to configure *EditionID* again.

## Installing Docker
To install Docker, we download the executable available in [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-windows/).

![Docker Install](/images/docker-install-win10/d2.png)

Once Docker is downloaded, **we run the installer as administrator**.

Docker will start to run. When the installation is complete the computer will restart.

![Docker Installed](/images/docker-install-win10/d3.png)

**Great Job!, Docker has been installed.**

### Docker Testing

If you want to test the docker installation, you can use the following docker command.

```shell
docker run hello-world
```
![Docker Output](/images/docker-install-win10/d4.png)

> **If you liked the guide, share it and let me know, if you find any mistakes you can tell me we are here to learn from our mistakes and good luck.**

You can also invite me for a coffee ☕️ [Paypal](https://paypal.me/shernandezlara)

Thanks! ❤️
